from save_data_to_csv import save_dataset
from tech_ind_model import tech_ind_model
from basic_model import basic_model
from trading_algo import trading_algo
import tensorflow as tf
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument('symbol', type=str, help="the stock symbol you want to download")
    parser.add_argument('time_window', type=str, choices=[
                        'intraday', 'daily', 'daily_adj'], help="the time period you want to download the stock history for")
    parser.add_argument('model_name', type=str, help="the model to use: basic | technical")
    parser.add_argument('multi_stock_training', type=str, help="train with multiple stocks: true | false")

    namespace = parser.parse_args()

    # print("test gpu:")
    # print(f"gpu v1: ${tf.compat.v1.test.is_gpu_available}")
    # print(f"gpu v2: ${tf.compat.v2.test.is_gpu_available}")


    # tf.test.is_gpu_available(
    #     cuda_only=False,
    #     min_cuda_compute_capability=None
    # )

    print(f"args: ${namespace.symbol}")
    # save_dataset(**vars(namespace))
    save_dataset(namespace.symbol, namespace.time_window)
    if namespace.model_name == 'basic':
        basic_model(namespace.symbol, namespace.time_window)
    elif namespace.model_name == 'technical':
        tech_ind_model(namespace.symbol, namespace.time_window, namespace.multi_stock_training)
    trading_algo(**vars(namespace))