#!/bin/bash

conda install -y -c conda-forge keras
conda install -y -c anaconda pandas
conda install -y -c anaconda scikit-learn
conda install -y -c conda-forge matplotlib
pip install alpha_vantage
#conda install -c hoishing alpha_vantage
#conda install -y -c r r-alphavantageclient
#conda install -y -c conda-forge r-alphavantager