FROM continuumio/miniconda3
RUN conda create -n env python=3.7
RUN echo "source activate env" > ~/.bashrc
ENV PATH /opt/conda/envs/env/bin:$PATH

RUN mkdir /opt/stock-trading-ml
#RUN mkdir /opt/stock-trading-ml/graphs

#RUN conda install -y -c conda-forge keras
#RUN conda install -y -c anaconda pandas
#RUN conda install -y -c anaconda scikit-learn
#RUN conda install -y -c conda-forge matplotlib
#RUN pip install alpha_vantage
#RUN conda install -c r r-alphavantageclient

#RUN cd /tmp/
#RUN git clone git@bitbucket.org:jonpoi/stock-trading-ml.git
#RUN git clone https://jonpoi@bitbucket.org/jonpoi/stock-trading-ml.git
#RUN cd stock-trading-ml
