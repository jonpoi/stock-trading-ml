# Create docker image
docker build -t stml .
# Run docker image
docker run -v "$PWD:/opt/stock-trading-ml" -it stml
# Go to code directory
cd opt/stock-trading-ml
# Install Python dependencies
sh environment.sh
# Run program
python main.py TSLA daily technical false
